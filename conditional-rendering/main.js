function UserGreeting(props) {
    return <h1>Wellcame To Deshboard</h1>;
}

function GestGreeting(props) {
    return <h1>sign up please</h1>;
}

function Greeting(props) {
    const isLoggedIn = props.isLoggedIn;
    if (isLoggedIn) {
        return <UserGreeting />;
    }
    return <GestGreeting />;
}

ReactDOM.render(
    <Greeting isLoggedIn={true}/>,
    document.getElementById('root')
)
