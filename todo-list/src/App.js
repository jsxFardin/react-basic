import React, { Component } from 'react';
import TodoList from './component/TodoList';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="text-center">
          <h3>TODO LIST</h3>
        </div>
        <TodoList/>
      </div>
    );
  }
}

export default App;
