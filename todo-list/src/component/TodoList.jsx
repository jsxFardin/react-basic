import React, { Component } from 'react';
import '../App.css';

class TodoList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            userInput: '',
            list: []
        }
    }
    changeValue(value) {
        this.setState({
            userInput: value
        });
    }
    addToList(item) {
        if (item) {
            let listArr = this.state.list;
            listArr.push(item);

            this.setState({
                list: listArr,
                userInput: ''
            })
        }
        else{
            alert('enter someting');
        }
    }
    closeRow(input) {
        let listArr = this.state.list;
        let filterArr = listArr.filter(e => e !== input);

        this.setState({
            list: filterArr,
            userInput: ''
        });
    }
    render() {
        return (
            <div className="header">
                <input
                    type="text"
                    value={this.state.userInput}
                    onChange={(e) => this.changeValue(e.target.value)}
                />
                <button className="addBtn" onClick={() => this.addToList(this.state.userInput)} >add</button>
                <br></br>
                <br></br>
                <ul>
                    {this.state.list.map((val) => {
                        return (
                            <li key={val.toString()} className="text-left">
                                {val}
                                <span onClick={() => this.closeRow(val)} className="close">x</span>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}

export default TodoList;