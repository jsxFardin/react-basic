
/* 
* a class like a plean of anything what you want do. in javascript class is special function.

* extands is a keyword. Here in the line 12 extends means that HolloMessage inherits all property of 
    React.Component. It's like a relationship with father and son. here React.Component is father and 
    HolloMessage is son.

* Component is a building block of any react app a typical React app will have many of these. 
    React.Component is a subclass of react.
*/
class HelloMessage extends React.Component {
    /*
        render() method that takes input data and returns what to display.
    */
    render() {
        return (
            <div>
                Hello {this.props.name}
            </div>
        );
    }
}

/* 
    * ReactDOM create a virtual Dom. Whenever we change or update a componets it compair with virtual dom 
        and if virtual dom find any changes/update ? Then it changes into actual dom.
   
    * render method returns a description of what you want to see on the screen.

    * HelloMessage is a react component. Here we call it like xml tag. It's call jsx(javscript xml).
        it has name attribute.
    
    * document.getElementById('root') indicate the container in html page.
*/

ReactDOM.render(
    <HelloMessage name="Fardin" />,
    document.getElementById('root')
);